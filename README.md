# TemporaryTestProject

[Git Town](https://www.git-town.com) works well.

But, *git town ship* squashes the merge and closes the merge request instead of merging it. So, while that might work for GitHub, it doesn't for GitLab.

After successful merge that also deleted the branch on GitLab, to sync the state with your checkout, you have to run *git town prune-branches* instead of *git town sync*.
